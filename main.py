from web import api, subscribe_handler
from tornado.wsgi import WSGIContainer
from tornado.web import Application, FallbackHandler
from tornado.ioloop import IOLoop
import sys

if __name__ == "__main__":
    settings = {'debug' : True}
    wsgi_app = WSGIContainer(api)
    application = Application([
        (r'/subscribe', subscribe_handler),
        (r'.*', FallbackHandler, dict(fallback=wsgi_app))
    ], **settings)

    application.listen(5000, address="0.0.0.0")
    IOLoop.instance().start()

