
function requestCreateComponent(id, module_type, class_type, callback){
	var http = new XMLHttpRequest();
    var url = '/component?id=' + id + '&name=' + name + '&type=' + module_type + '.' + class_type;
    http.onreadystatechange = function(){
        if(http.readyState == 4){
            callback(http.responseText, http.status);	
        }
    }
    http.open("POST", url);
    http.send(null);
}

function requestLoadComponent(id, step, callback){
	var http = new XMLHttpRequest();
    var url = '/components/' + id + '?step=' + step;
    http.onreadystatechange = function(){
        if(http.readyState == 4){
            callback(http.responseText, http.status);	
        }
    }
    http.open("GET", url);
    http.send(null);
}

function requestDeleteLink(id, source_name, target_name, callback){
	var http = new XMLHttpRequest();
    var url = '/link/' + id + '/' + source_name + '/' + target_name
    http.onreadystatechange = function(){
        if(http.readyState == 4){
            callback(http.responseText, http.status);	
        }
    }
    http.open("DELETE", url);
    http.send(null);
}

function requestDeleteComponent(id, callback){
	var http = new XMLHttpRequest();
    var url = '/components/' + id;
    http.onreadystatechange = function(){
        if(http.readyState == 4){
            callback(http.responseText, http.status);	
        }
    }
    http.open("DELETE", url);
    http.send(null);
}

function requestUpdateComponent(id, name, value, callback){
	var http = new XMLHttpRequest();
    var url = '/property/' + id;
    http.onreadystatechange = function(){
        if(http.readyState == 4){
            callback(http.responseText, http.status);	
        }
    }
    var json = [{'name': name, 'value': value}]
    http.open("POST", url);
    http.setRequestHeader('content-type', 'application/json');
    http.send(JSON.stringify(json));
}

function requestCreateLink(srcId, srcSlot, tarId, tarSlot, callback) {
    var http = new XMLHttpRequest();
    var url = '/links?src=' + srcId + '&srcSlot=' + srcSlot + '&tar=' + tarId + "&tarSlot=" + tarSlot;
    http.onreadystatechange = function(){
        if(http.readyState == 4){
            callback(http.responseText, http.status);	
        }
    }
    http.open("GET", url);
    http.send(null);
}

function requestSingleModule(module_name, callback){
	var http = new XMLHttpRequest();
    var url = '/module/' + module_name;
    http.onreadystatechange = function(){
        if(http.readyState == 4){
            callback(http.responseText, http.status);	
        }
    }
    http.open("GET", url);
    http.send(null);
}

function requestComponentUI(id, callback){
    var http = new XMLHttpRequest();
    var url = '/componentui/' + id;
    http.onreadystatechange = function(){
        if(http.readyState == 4){
            callback(http.responseText, http.status);	
        }
    }
    http.open("GET", url);
    http.send(null);
}

function requestMenuAction(id, name, value, callback){
	var http = new XMLHttpRequest();
    var url = '/menu/' + id;
    http.onreadystatechange = function(){
        if(http.readyState == 4){
            callback(http.responseText, http.status);	
        }
    }
    var json = {'name': name, 'value': value}
    http.open("POST", url);
    http.setRequestHeader('content-type', 'application/json');
    http.send(JSON.stringify(json));
}

function requestLoadMenu(id, callback){
	var http = new XMLHttpRequest();
    var url = '/menu/' + id;
    http.onreadystatechange = function(){
        if(http.readyState == 4){
            callback(http.responseText, http.status);	
        }
    }
    http.open("GET", url);
    http.send(null);
}

function requestFilterType(filter_module, filter_class, target_module, callback){
	var http = new XMLHttpRequest();
    var url = '/type_filter';
    http.onreadystatechange = function(){
        if(http.readyState == 4){
            callback(http.responseText, http.status);
        }
    }
    var json = {'filter_module': filter_module, 'filter_class': filter_class, 'target_module':target_module}
    http.open("POST", url);
    http.setRequestHeader('content-type', 'application/json');
    http.send(JSON.stringify(json));
}

function requestFilterAllType(module, class_type, callback){
	var http = new XMLHttpRequest();
    var url = '/type_filter_all';
    http.onreadystatechange = function(){
        if(http.readyState == 4){
            callback(http.responseText, http.status);
        }
    }
    var json = {'module': module, 'class': class_type}
    http.open("POST", url);
    http.setRequestHeader('content-type', 'application/json');
    http.send(JSON.stringify(json));
}

function requestGetDevices(id, callback){
	var http = new XMLHttpRequest();
    var url = '/devices/' + id;
    http.onreadystatechange = function(){
        if(http.readyState == 4){
            callback(http.responseText, http.status);	
        }
    }
    http.open("GET", url);
    http.send(null);
}

function requestGetNetwork(id, callback){
	var http = new XMLHttpRequest();
    var url = '/networks/' + id;
    http.onreadystatechange = function(){
        if(http.readyState == 4){
            callback(http.responseText, http.status);	
        }
    }
    http.open("GET", url);
    http.send(null);
}

function JQueryDataGridToValue(data, row){
    if(data.editor.type == 'combobox'){
        for(i in data.editor.options.data){
            var obj = data.editor.options.data[i];
            if(row.value == obj['text']){
                return {'id': data.handle, 'name': row.id, 'value': obj['value']};
            }   
        }
    } else if(data.editor.type == 'datebox'){
        var value = data.value;
        var date = value.split('/');
        var res = {
            'name': row.id, 
            'id': data.handle, 
            'value': { 
                'type': 'component.Date', 
                'value': {
                    'year': parseInt(date[2]), 
                    'month': parseInt(date[1]), 
                    'day': parseInt(date[0]) 
                         } 
                     }
                 };
        return res;
    } else if(data.editor.type == 'timespinner'){
        var value = data.value;
        var date = value.split(':');
        return {
                'name': row.id,
                'id': data.handle,
                'value':{
                    'type': 'component.Time',
                    'value': {
                        'hour': parseInt(date[0]),
                        'minute': parseInt(date[1]),
                        'second': 0
                    }
            }
        }
    } else {
        return {
            'name': row.id,
            'id': data.handle,
            'value': data.value
        }
    }
}

function encodeToJQueryDataGrid(group, properties){
	var obj = {};
	
	obj.group = group;
    obj.handle = 0;

	var prop = properties;
	var propName = prop.name;
	var propFlag = prop.flag;
	var propValue = prop.value;

	obj.id = propName;
	obj.name = propName.toLowerCase().replace(/( |^)[a-z]/g, (L) => L.toUpperCase()).replace('_', ' ');

	if( propFlag.indexOf('h') != -1)
	    return;

	if( propFlag.indexOf('r') != -1){
	    obj.value = propValue;
	    obj.styler = 'background-color:#ffee00;color:gray;';
	    obj.editor = {
	        'type': 'textbox',
	        'options': {'readonly':true}
	    }
	} else if( typeof(propValue) == 'number' ){
	    obj.value = propValue;
	    obj.editor = {
	        'type': 'numberbox'
	    }
	} else if( typeof(propValue) == 'boolean'){
	    obj.value = '' + propValue;
	    obj.editor = {
	        'type':'combobox',
	        'options':{
	            'editable': false,
	            'data':[
	                {
	                    'value': true,
	                    'text':'true',
	                },
	                {
	                    'value': false,
	                    'text': 'false'
	                }
	            ]
	        }
	    }
	} else if( typeof(propValue) == 'string'){
		obj.value = propValue;
		obj.editor = {'type': 'text'};
	} else {
		var type = propValue.type;
		var value = propValue.value;
		if(type == 'component.Time'){
			obj.value = '' + value.hour + ':' + value.minute
			obj.editor = {
	            'type':'timespinner',
	            'options':{
	            	'value': '' + value.hour + ':' + value.minute,
	                'required': true
	            }
	        }
		} else if(type == 'component.Date'){
			obj.value = '' + value.day + '-' + value.month + '-' + value.year;
			obj.editor = {
	            'type':'datebox',
	            'options':{
	            	'value': obj.value,
	                'required': true
	            }
	        }
		} else if(type == 'component.EnumValue'){
			var select = propValue.select;
			obj.value = select;
            var local_data = new Array();
            for(i in value)
                local_data.push({'value': value[i], 'text': '' + value[i]})
            obj.editor = {
                'type':'combobox',
                'options':{
                    'editable': false,
                    'data':local_data,
                    'valueField': "value",
                    'textField': "text"
                }
            }
		}
	}
	return obj;
}


$.extend($.fn.datagrid.defaults.editors, {
    timespinner: {
        init: function (container, options) {
            var input = $('<input />').appendTo(container);
            input.timespinner(options);
            return input;
        },
        getValue: function (target) {
            return $(target).timespinner('getValue');
        },
        setValue: function (target, value) {
            $(target).timespinner('setValue', value);
        },
        resize: function (target, width) {
            var input = $(target);
            if ($.boxModel == true) {
                input.resize('resize', width - (input.outerWidth() - input.width()));
            } else {
                input.resize('resize', width);
            }
        }
    }
});

$.extend($.fn.datagrid.defaults.editors, {
	datebox : {
		init : function(container, options) {
			var input = $('<input type="text">').appendTo(container);
			input.datebox(options);
			return input;
		},
		destroy : function(target) {
			$(target).datebox('destroy');
		},
		getValue : function(target) {
			return $(target).datebox('getValue');//获得旧值
		},
		setValue : function(target, value) {
			$(target).datebox('setValue', value);//设置新值的日期格式
		},
		resize : function(target, width) {
			$(target).datebox('resize', width);
		}
	}
});


